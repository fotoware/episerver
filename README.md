# README #

The FotoWeb EPiServer plugin let you insert images from inside FotoWeb to your EPiServer pages.
To learn more about the project, go to [this webpage](http://learn.fotoware.com/02_FotoWeb_8.0/Integrating_FotoWeb_with_CMS_systems/FotoWeb_EPiServer_Plugin)

### What is this repository for? ###

* Anyone who wants to import images from FotoWeb to EPiServer
* Version 1.0.0.1

### How do I get set up? ###

To get set up, please read the [following article](http://learn.fotoware.com/02_FotoWeb_8.0/Integrating_FotoWeb_with_CMS_systems/Installing_the_FotoWeb_plugin_for_EpiServer)

### Contribution guidelines ###

Overview
The FotoWeb EPiServer plugin is an Open Source project under the AGPL license.

Anyone can clone and fork this project. But any changes needs to be contributed back with a pull request.

### Who do I talk to? ###

If you have any questions about this plugin, please contact [mh@fotoware.com](mailto:mh@fotoware.com)